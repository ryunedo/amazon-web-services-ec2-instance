# Training to use Amazon Web Services
## _Using AWS EC2_


### Amazon Elastic Compute Cloud (EC2)
>Amazon Elastic Compute Cloud (Amazon EC2)  is a web service that provides secure, resizable compute capacity in the cloud. EC2 offers many options that enable you to build and run virtually any application. With these possibilities, getting started with EC2 is quick and easy to do.

## Overview Instance
| Launch an instance | Type |
| ------ | ------ |
| Name And Tag | ryann |
| Application and OS Images (Amazon Machine Image) | Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type |
| Instance type | t2.micro |
| Key pair | unedoo.ppk |
| Network settings(Security Group) | launch-wizard-3 |
| Configure storage | Default  |
| Advanced details | Default (except User data) |


# More Details
## name tag : ryann 

## Application an OS Image (Amazon Machine Image) 
> Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type
>Chosen because this one is Free tier eligible 

## Instance Type
> t2.micro
>Chosen because this one is Free tier eligible 
>And have specification : -1CPU dan 1GB Memory


## Key Pair
>Using Key pair, make our instance more secure if using key pair


## Network settings
|   |   |
| ------ | ------ |
| VPC | Default |
|Subnet | No Preference |
| Auto-assign public IP | Enable |
| Firewall ( Security Group) | launch-wizard-3 |
| Security Group Name | launch-wizard-3 |
> Security Group launch wizard 3 Dengan Spesifikasi : 
> - Allow ssh traffic from anywhere 
>- Allow https traffic from internet
> - Allow http traffic from internet


> Allow all traffic because its only for testing and everyone can access from http or ssh

## Configure stroage
> default = storage 1x8gb and root volume general purpose ssd (gp2)

## Advanced detail
> All default except "user data"

## User Data from Advanced Detail : 
yum command is utility which can install, erase, or looking for packages
```sh
#!/bin/bash
yum -y install httpd
systemctl enable httpd
systemctl start httpd
echo '<html><h2>Hello World</h2></html>'
 > /var/www/html/index.html
```

-y command is a description for "yes" in the installation section httpd or apache
```ssh
yum -y install httpd
```

Next command is used to turn on apache or httpd
```ssh
systemctl enable httpd 
```

Next command is used to start apache or httpd
```ssh
systemctl start httpd 
```

Command echo is used to print text on screen
```ssh
echo '<html><h1>Hello World</h1></html>'
```

Command ">" is using to know where file is located
```ssh
 > /var/www/html/index.html
 ```
 
## Credit
### Febryano Unedo Lbn Siantar